package com.transit.monitor.util;

import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

@Component
public class RestUtils {

	/**
	 * This method consumes RESTful API
	 * 
	 * @param serviceUrl
	 * @return String - restTemplate response
	 */

	public String getTemplateResponse(String serviceUrl) {
		System.out.println("URL: " + serviceUrl);
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
				HttpClientBuilder.create().build());
		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.add("Content-Type", "application/json");
		HttpEntity<String> entity = new HttpEntity<>(requestHeaders);
		String response = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, String.class).getBody();
		return response;
	}

}
