package com.transit.monitor.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.transit.monitor.service.ElasticSearchService;
import com.transit.monitor.service.Transit511Service;

@RestController
public class TransitInfoController {

	@Autowired
	private ElasticSearchService elasticSearchService;

	@Autowired
	private Transit511Service transit511Service;

	/**
	 * This method returns AgencyName, stopCode and Estimated arrival time
	 * 
	 * @param stopCode
	 */
	@RequestMapping(value = "api/v1/stopInfo/{stopCode}")
	public String getStopInfo(@PathVariable int stopCode) {
		String elasticSearchResult = elasticSearchService.getAgency(stopCode);
		String transitResult = transit511Service.getTransitData(elasticSearchResult, stopCode);
		return transitResult;
	}
}
