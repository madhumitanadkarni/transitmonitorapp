package com.transit.monitor.app;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages={"com.transit.monitor"})
@EnableAutoConfiguration
public class TransitMonitorAppApplication {

	public static void main(String[] args) {
		SpringApplication.run(TransitMonitorAppApplication.class, args);
	}
}
