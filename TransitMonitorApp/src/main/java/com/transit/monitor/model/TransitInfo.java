package com.transit.monitor.model;


public class TransitInfo {

	private String agency;
	private int stopCode;
	private String estimatedArrivalTime;
	private String busNumber;
	private String error;

	public String getAgency() {
		return agency;
	}

	public void setAgency(String agency) {
		this.agency = agency;
	}

	public int getStopCode() {
		return stopCode;
	}

	public void setStopCode(int stopCode) {
		this.stopCode = stopCode;
	}

	public String getEstimatedArrivalTime() {
		return estimatedArrivalTime;
	}

	public void setEstimatedArrivalTime(String estimatedArrivalTime) {
		this.estimatedArrivalTime = estimatedArrivalTime;
	}

	public String getBusNumber() {
		return busNumber;
	}

	public void setBusNumber(String busNumber) {
		this.busNumber = busNumber;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

	@Override
	public String toString() {
		return "TransitInfo [agency=" + agency + ", stopCode=" + stopCode + ", estimatedArrivalTime="
				+ estimatedArrivalTime + ", busNumber=" + busNumber + "]";
	}

}
