package com.transit.monitor.service;

import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import com.transit.monitor.util.RestUtils;

import org.json.JSONArray;

@Service
@Component
public class ElasticSearchServiceImpl implements ElasticSearchService {

	@Autowired
	private RestUtils util;

	@Value("${api.elasticsearch}")
	private String esUrl;

	@Override
	public String getAgency(int stopCode) {
		String url = esUrl + "/_search?q=stopCode:" + stopCode;
		String jsonString = util.getTemplateResponse(url);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONArray jsonArray = jsonObject.getJSONObject("hits").getJSONArray("hits");
		String agency = null;
		if (jsonArray.length() > 0) {
			agency = jsonArray.getJSONObject(0).getJSONObject("_source").getString("agency");
		}
		return translateAgency(agency);
	}

	private static String translateAgency(String agency) {
		if (agency == null){
			return null;
		}
		switch (agency) {
		case "AC Transit":
			return "actransit";
		case "SFMTA":
			return "sf-muni";
		default:
			return agency;
		}
	}
}
