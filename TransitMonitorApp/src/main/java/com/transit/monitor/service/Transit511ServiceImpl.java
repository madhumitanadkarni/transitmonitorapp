package com.transit.monitor.service;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.transit.monitor.model.TransitInfo;
import com.transit.monitor.util.RestUtils;

@Service
@Component
public class Transit511ServiceImpl implements Transit511Service {

	@Value("${api.key.token}")
	private String token;

	@Value("${api.stop.monitoring}")
	private String api;

	@Autowired
	private RestUtils util;

	@Override
	public String getTransitData(String agency, int stopCode) {

		List<String> resultArray = new ArrayList<String>();
		Gson gson = new Gson();

		if (agency == null) {
			TransitInfo transitInfo = new TransitInfo();
			transitInfo.setError("Invalid Stop Code: "+stopCode);
			String json = gson.toJson(transitInfo);
			resultArray.add(json);
			return resultArray.toString();
		}

		String url = api + "?api_key=" + token + "&agency=" + agency + "&stopCode=" + stopCode;
		String jsonString = util.getTemplateResponse(url).substring(1);
		JSONObject jsonObject = new JSONObject(jsonString);
		JSONArray jsonArray = jsonObject.getJSONObject("ServiceDelivery").getJSONObject("StopMonitoringDelivery")
				.getJSONArray("MonitoredStopVisit");

		for (int i = 0; i < jsonArray.length(); i++) {
			if (i == 2) {
				break;
			}
			String estimatedArrivalTime = jsonArray.getJSONObject(i).getJSONObject("MonitoredVehicleJourney")
					.getJSONObject("MonitoredCall").getString("AimedArrivalTime");
			String busNumber = jsonArray.getJSONObject(i).getJSONObject("MonitoredVehicleJourney").getString("LineRef");
			TransitInfo transitInfo = new TransitInfo();
			transitInfo.setAgency(agency);
			transitInfo.setBusNumber(busNumber);
			transitInfo.setEstimatedArrivalTime(estimatedArrivalTime);
			transitInfo.setStopCode(stopCode);
			String json = gson.toJson(transitInfo);
			resultArray.add(json);
		}
		return resultArray.toString();
	}

}
