package com.transit.monitor.service;

import org.springframework.stereotype.Component;

@Component
public interface Transit511Service {

	/**
	 * This method returns Transit information JSON string containing agency,
	 * stopCode, busNumber, estimatedArrivalTime for first two buses to arrive
	 * 
	 * @param agency
	 * @param stopCode
	 * @return
	 */
	public String getTransitData(String agency, int stopCode);

}
