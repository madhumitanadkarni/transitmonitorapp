package com.transit.monitor.service;

import org.springframework.stereotype.Component;

@Component
public interface ElasticSearchService {
	
	/**
	 * This method queries ElasticSearch and returns the Agency name corresponding to the stop code
	 * 
	 * @param stopCode
	 * @return agency name
	 */
	public String getAgency(int stopCode);

}
