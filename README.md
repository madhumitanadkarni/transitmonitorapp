# README #

## What is this repository for? ##

This is a system designed with a Service-oriented Architecture to receive requests from Slack Slash Command for SF Bay Area transit 
departure times, poll the 511 Transit API, parse the data, format the response, and send the correct response back to the Slack 
channel of the request.

This Repository contains two microservices:

* TransitMonitorApp: A generic SpringBoot microservice that provides Transit Info for arriving buses for a given 511 Stop.
* SlackTransitMonitorApp: A SpringBoot microservice that integrates with Slack API and uses TransitMonitorApp behind the scene.

## Using the pre-deployed service ##
* Join Transit Monitor Slack workspace to get access to the custom Slack Slash Command -- /slack511 [Stop-Code]
* Click the link below to join the slack workspace.
* <https://join.slack.com/t/madhumita-fsm/shared_invite/enQtMjYwMTEyMzczNTA2LTVhNjY5NjU3NDNhZjVlNjI1NzUxZWMwM2I0ZDZiMmZjZTc1OTg5ZGY5Mzk5MjY1YTBlZjY4NDVkMGMyMmM2OTI>
* Type /slack511 [Stop-Code]

## Setting up service from scratch ##

### Summary of set up ###
* Java 7 or above
* Maven: Build Tool
* SpringBoot Spring Tool Suite: IDE for development
* CloudFoundry: Platform for service deployment

### Deployment instructions ###
* Build JAR for the TransitMonitorApp:
	```
	cd TransitMonitorApp;
	mvn clean install
	```
* Deploy to CloudFoundry:
	```
	cf push -f manifest.yml
	```
* Update transit.monitor.url property in application.properties of SlackTransitMonitorApp with the TransitMonitorApp CloudFoundry URL.
* Build JAR for SlackTransitMonitorApp
	```
	cd SlackTransitMonitorApp; 
	mvn clean install
	```
* Deploy to CloudFoundry:
	```
	cf push -f manifest.yml
	```
* Create a Slack Application and point it to SlackTransitMonitorApp CloudFoundry URL and the end point - /api/v1/stopInfo
* Create a Slack Slash Command and point it to [SlackTransitMonitorApp CloudFoundry URL]/api/v1/stopInfo
** See <https://api.slack.com/apps> for more details.

### Who do I talk to? ###
* Madhumita Nadkarni - m.nadkarni26@gmail.com
