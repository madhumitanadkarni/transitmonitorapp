package com.transit.monitor.model;


public class TransitInfo {
	
	String agency;
	String stopCode;
	String estimatedArrivalTime;
	String busNumber;
	
	public String getAgency() {
		return agency;
	}
	public void setAgency(String agency) {
		this.agency = agency;
	}
	public String getStopCode() {
		return stopCode;
	}
	public void setStopCode(String stopCode) {
		this.stopCode = stopCode;
	}
	public String getEstimatedArrivalTime() {
		return estimatedArrivalTime;
	}
	public void setEstimatedArrivalTime(String estimatedArrivalTime) {
		this.estimatedArrivalTime = estimatedArrivalTime;
	}
	
	public String getBusNumber() {
		return busNumber;
	}
	public void setBusNumber(String busNumber) {
		this.busNumber = busNumber;
	}
	
	@Override
	public String toString() {
		return "TransitInfo [agency=" + agency + ", stopCode=" + stopCode + ", estimatedArrivalTime="
				+ estimatedArrivalTime + ", busNumber=" + busNumber + "]";
	}

}
