package com.transit.monitor.service;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.TimeZone;

import org.apache.commons.lang3.StringUtils;
import org.apache.http.impl.client.HttpClientBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.transit.monitor.model.TransitInfo;

import org.json.JSONArray;
import org.json.JSONObject;

@Service
@Component
public class TransitServiceImpl implements TransitService {


	@Value("${transit.monitor.url}")
	private String transitMonitorUrl;

	@Override
	public String getTransitData(String stopCode) {

		try {
			Integer.parseInt(stopCode);
		} catch (NumberFormatException e) {
			return getResult("Invalid stop code: " + stopCode);
		}
		
		String url = transitMonitorUrl + "/api/v1/stopInfo/" + stopCode;
		String response = getTemplateResponse(url);
		JSONArray jsonArray = new JSONArray(response);
		List<String> resultArray = new ArrayList<String>();
		for (int i = 0; i < jsonArray.length(); i++) {
			TransitInfo transitInfo = new TransitInfo();
			JSONObject jsonObject = jsonArray.getJSONObject(i);
			if (jsonObject.has("error")) {
				String errorMsg = jsonObject.getString("error"); 
				return getResult(errorMsg);
			}
			String agency = jsonObject.getString("agency");
			String busNumber = jsonObject.getString("busNumber");
			String arrivalTime = jsonObject.getString("estimatedArrivalTime");
			transitInfo.setAgency(agency);
			transitInfo.setBusNumber(busNumber);
			transitInfo.setEstimatedArrivalTime(arrivalTime);
			transitInfo.setStopCode(stopCode);
			String formattedArrivalTime = dateParser(arrivalTime);
			String result = agency + " buses arriving at stop " + stopCode + " : Bus " + busNumber + " at "
					+ formattedArrivalTime;
			resultArray.add(result);
		}
		String resultText = StringUtils.join(resultArray, "\n");
		if (resultArray.isEmpty()) {
			resultText = "No more buses arriving at stop " + stopCode;
		}
		return getResult(resultText);
	}
	
	private static String getResult(String resultText) {
		JSONObject jsonObject = new JSONObject().put("response_type", "ephemeral").put("text", resultText);
		return jsonObject.toString();
	}

	/**
	 * This method consumes RESTful API
	 * 
	 * @param serviceUrl
	 * @return String - restTemplate response
	 */
	private String getTemplateResponse(String serviceUrl) {
		System.out.println("URL: " + serviceUrl);
		HttpComponentsClientHttpRequestFactory clientHttpRequestFactory = new HttpComponentsClientHttpRequestFactory(
				HttpClientBuilder.create().build());
		RestTemplate restTemplate = new RestTemplate(clientHttpRequestFactory);
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.add("Content-Type", "application/json");
		HttpEntity<String> entity = new HttpEntity<>(requestHeaders);
		String response = restTemplate.exchange(serviceUrl, HttpMethod.GET, entity, String.class).getBody();
		return response;
	}

	/**
	 * This method converts time from "yyyy-MM-dd'T'HH:mm:ss'Z'" format in UTC
	 * TimeZone to "hh:mm a" format in PST TimeZone
	 * 
	 * @param time
	 * @return
	 */

	private String dateParser(String time) {
		DateFormat outputFormat = new SimpleDateFormat("hh:mm a");
		outputFormat.setTimeZone(TimeZone.getTimeZone("America/Los_Angeles"));
		DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
		inputFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		String formattedArrivalTime = null;
		try {
			Date date = inputFormat.parse(time);
			formattedArrivalTime = outputFormat.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return formattedArrivalTime;
	}

}
