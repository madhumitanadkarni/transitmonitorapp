package com.transit.monitor.service;

import org.springframework.stereotype.Component;

@Component
public interface TransitService {
	
	/**
	 * This method returns JSON formatted Transit Info for the given stop code as expected by Slack. 
	 * In particular, includes the following two fields:
	 * "response_type"
	 * "text"
	 * @param stopCode
	 *
	 */
	public String getTransitData(String stopCode);

}
