package com.transit.monitor.controller;

import java.nio.file.AccessDeniedException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.transit.monitor.service.TransitService;

@RestController
public class SlackController {

	@Value("${verification.token}")
	private String desiredToken;

	@Autowired
	private TransitService transitService;

	@RequestMapping(value = "/api/v1/stopInfo", method = RequestMethod.POST, produces = "application/json")
	public String stopInfo(@ModelAttribute("token") String token, @ModelAttribute("text") String stopCode)
			throws AccessDeniedException {

		if (!desiredToken.equals(token)) {
			throw new AccessDeniedException("forbidden");
		}

		if (stopCode == null || stopCode.isEmpty()) {
			return "Invalid command. Please add Stop Code.";
		}

		String response = transitService.getTransitData(stopCode);
		return response;
	}
}
